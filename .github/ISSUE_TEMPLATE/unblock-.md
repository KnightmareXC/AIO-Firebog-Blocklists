---
name: 'Unblock '
about: 'Template to suggest domains to unblock '
title: Domain(s) blocked by [List Name]
labels: ''
assignees: KnightmareVIIVIIXC

---

### Domain Unblocking Request

#### Domain(s) in Question
List the domain(s) that you believe should be unblocked.

- [ ] Domain 1
- [ ] Domain 2
- [ ] ...

#### Justification
Provide a brief explanation of why you think the specified domain(s) should be unblocked. Include any relevant context or evidence to support your request.

#### Additional Information
Add any supplementary information or details that might be useful for reviewing and processing this request.

#### Checklist
Before submitting this issue, make sure you have completed the following:

- [ ] Searched for existing issues related to this domain unblocking request.
- [ ] Ensured that the request aligns with the project's guidelines and policies.

#### Declaration
By submitting this request, I confirm that the information provided is accurate and that I have read and understood the project's domain unblocking policies.
