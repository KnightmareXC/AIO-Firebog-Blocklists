<div align="center">

# 🔴 The Firebog ➖ DNS Blocklists 🔴

![](https://github.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/assets/114373431/90b2df95-5a0d-4975-9233-68e4bd21d722)

***Diverse DNS blocklists crafted from<br>an amalgamation of sources<br>and intended to***

| ⚪ | ⚪ ⚪ ⚪ |
|:---:|:---|
| ⚪ |Prevent Advertisements|
| ⚪ |Halt Trackers|
| ⚪ |Block Malicious Content|
| ⚪ |Stop Phishing Attempts|
| ⚪ |Disable Crypto Miners|
| ⚪ |Hide Adult Content|

---

## 📄 The Lists 📄

***Blocklists [designed](https://github.com/KnightmareVIIVIIXC/DeadDomainsLinter) in the style of<br><a href="https://adguard-dns.io/kb/general/dns-filtering-syntax/#adblock-style-syntax" title="">adblock</a>, <a href="https://adguard-dns.io/kb/general/dns-filtering-syntax/#domains-only-syntax" title="">domain-only</a>, & <a href="https://adguard-dns.io/kb/general/dns-filtering-syntax/#etc-hosts-syntax" title="">hosts</a>,<br>meant to be used with***

| ⚪ | ⚪ ⚪ ⚪ |
|:---:|:---|
| ⚪ |AdBlockers|
| ⚪ |DNS Applications|
| ⚪ |Windows' Hosts File| 

| List | ⚪ | Description |
|---:|:---:|:---|
|Bog| 🔥 |The Bog All-In-One (AIO) list encompasses all sources & will disrupt your browsing experience, consider implementing an allowlist for better control|
|Blue/Green| 🔷 |Incorporates blue sources & green sources, included in The Bog AIO list & blue/green AIO list, poses a higher risk of disrupting your browsing experience|
|Blue| 🟦 |Incorporates blue sources, included in The Bog AIO list & the blue AIO list, poses a higher risk of disrupting your browsing experience|
|Green| 🟩 |Incorporates green sources, included in The Bog AIO list & the green AIO list, poses a lower risk of disrupting your browsing experience, excludes [anudeepND's Allowlist](https://github.com/KnightmareVIIVIIXC/allowlist)|

### ⭕ AIO Lists ⭕

| 🔥 🟦 🟩 | ⚪ | AdBlock | PiHole | Hosts | Domain | 🟩 🟦 🔥 |
|---:|:---:|:---:|:---:|:---:|:---:|:---|
| The Bog AIO List | 🔥 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/aiofirebog.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/aiofirebog.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/aiofirebog.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/aiofirebog.txt" title="">RAW</a> | Contains all of the sources |
| Blue/Green AIO List | 🔷 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/aiofirebogbluegreen.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/aiofirebogbluegreen.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/aiofirebogbluegreen.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/aiofirebogbluegreen.txt" title="">RAW</a> | Contains all of the blue/green sources |
| Blue AIO List| 🟦 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/aiofirebogblue.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/aiofirebogblue.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/aiofirebogblue.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/aiofirebogblue.txt" title="">RAW</a> | Contains all of the blue sources |
| Green AIO List | 🟩 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/aiofireboggreen.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/aiofireboggreen.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/aiofireboggreen.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/aiofireboggreen.txt" title="">RAW</a> | Contains all of the green sources |

### 💡 Lite Lists 💡

| 🔷 🔷 🔷 | ⚪ | 🔷 | 🔷 | 🔷 | 🔷 | 🔷 🔷 🔷 |
|---:|:---:|:---:|:---:|:---:|:---:|:---|
| Suspicious-Malicious | 🔷 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogsusmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogsusmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogsusmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogsusmal.txt" title="">RAW</a> | Contains the blue/green suspicious & malicious sources |
| Advertising-Tracking | 🔷 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogadtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogadtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogadtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogadtrack.txt" title="">RAW</a> | Contains the blue/green advertising & tracking sources |
| Advertising-Tracking-Malicious | 🔷 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogadtrackmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogadtrackmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogadtrackmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogadtrackmal.txt" title="">RAW</a> | Contains the blue/green advertising, tracking, & malicious sources |
| Suspicious | 🔷 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogsus.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogsus.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogsus.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogsus.txt" title="">RAW</a> | Contains the blue/green suspicious sources |
| Advertising | 🔷 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogad.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogad.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogad.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogad.txt" title="">RAW</a> | Contains all of the advertising sources |
| Tracking | 🔷 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogtrack.txt" title="">RAW</a> | Contains the blue/green tracking sources |
| Malicious | 🔷 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogmal.txt" title="">RAW</a> | Contains all of the malicious sources |
| 🟦 🟦 🟦 | ⚪ | 🟦 | 🟦 | 🟦 | 🟦 | 🟦 🟦 🟦 |
| Suspicious-Malicious | 🟦 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogbluesusmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogbluesusmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogbluesusmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogbluesusmal.txt" title="">RAW</a> | Contains the blue suspicious & malicious sources |
| Advertising-Tracking | 🟦 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogblueadtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogblueadtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogblueadtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogblueadtrack.txt" title="">RAW</a> | Contains the blue advertising & tracking sources |
| Advertising-Tracking-Malicious | 🟦 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/firebogblueadtrackmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/firebogblueadtrackmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/firebogblueadtrackmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/firebogblueadtrackmal.txt" title="">RAW</a> | Contains the blue advertising, tracking, & malicious sources |
| 🟩 🟩 🟩 | ⚪ | 🟩 | 🟩 | 🟩 | 🟩 | 🟩 🟩 🟩 |
| Suspicious-Malicious | 🟩 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/fireboggreensusmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/fireboggreensusmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/fireboggreensusmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/fireboggreensusmal.txt" title="">RAW</a> | Contains the green suspicious & malicious sources |
| Advertising-Tracking | 🟩 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/fireboggreenadtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/fireboggreenadtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/fireboggreenadtrack.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/fireboggreenadtrack.txt" title="">RAW</a> | Contains the green advertising & tracking sources |
| Advertising-Tracking-Malicious | 🟩 | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/lists/fireboggreenadtrackmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/phlists/fireboggreenadtrackmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/hostslists/fireboggreenadtrackmal.txt" title="">RAW</a> | <a href="https://raw.githubusercontent.com/KnightmareVIIVIIXC/AIO-Firebog-Blocklists/main/domlists/fireboggreenadtrackmal.txt" title="">RAW</a> | Contains the green advertising, tracking, & malicious sources |

---

## 📛 The Sources 📛

***All sources can be found on<br><a href="https://v.firebog.net" title="">The Firebog</a> hosted by <a href="https://github.com/WaLLy3K" title="">WaLLy3K</a>***

| List | ⚪ | Description |
|---:|:---:|:---|
|Bog| 🔥 |Exclusive to The Bog All-In-One (AIO) list|
|Blue| 🟦 |Featured in The Bog AIO list & blue lists|
|Green| 🟩 |Featured in The Bog AIO list & green lists|

| ⚪ ⚪ ⚪ | ⚪ | Source |
|---:|:---:|:---|
| Suspicious | ⚪ | 🔥 🟦 🟩 |
| ⚪ | 🟩 | <a href="https://v.firebog.net/hosts/static/w3kbl.txt" title="">WaLLy3K's Blocklist</a> |
| ⚪ | 🔥 | <a href="https://hostsfile.mine.nu/hosts0.txt" title="">The Hosts File Project</a> |
| ⚪ | 🔥 | <a href="https://adblock.mahakala.is/" title="">Mahakala</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/PolishFiltersTeam/KADhosts/master/KADhosts.txt" title="">PolishFiltersTeam KADhosts</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Spam/hosts" title="">Fademind's Spammers</a> |
| ⚪ | 🟦 | <a href="https://raw.githubusercontent.com/matomo-org/referrer-spam-blacklist/master/spammers.txt" title="">Matomo Referrer Spam</a> |
| ⚪ | 🟦 | <a href="https://someonewhocares.org/hosts/zero/hosts" title="">Dan Pollock's List</a> |
| ⚪ | 🟦 | <a href="https://raw.githubusercontent.com/VeleSila/yhosts/master/hosts" title="">VeleSila yhosts</a> |
| ⚪ | 🟦 | <a href="https://winhelp2002.mvps.org/hosts.txt" title="">MVPS Hosts</a> |
| ⚪ | 🟦 | <a href="https://cdn.jsdelivr.net/gh/neoFelhz/neohosts@gh-pages/full/hosts.txt" title="">neoFelhz's neoHosts</a> |
| ⚪ | 🟦 | <a href="https://raw.githubusercontent.com/RooneyMcNibNug/pihole-stuff/master/SNAFU.txt" title="">RooneyMcNibNug's SNAFU list</a> |
| ⚪ | 🟦 | <a href="https://paulgb.github.io/BarbBlock/blacklists/domain-list.txt" title="">paulgb's BarbBlock</a> |
| Advertising | ⚪ | 🟦 🟩 🟩 |
| ⚪ | 🟩 | <a href="https://adaway.org/hosts.txt" title="">AdAway</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/LanikSJ/ubo-filters/main/filters/getadmiral-domains.txt" title="">LanikSJ's GetAdmiral</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt" title="">Anudeep ND's Blocklist</a> |
| ⚪ | 🟩 | <a href="https://pgl.yoyo.org/adservers/serverlist.php?hostformat=nohtml" title="">Peter Lowe's Adservers</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/FadeMind/hosts.extras/master/UncheckyAds/hosts" title="">Fademind's Unchecky Ads</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/bigdargon/hostsVN/master/hosts" title="">hostsVN</a> |
| ⚪ | 🟦 | <a href="https://raw.githubusercontent.com/jdlingyu/ad-wars/master/hosts" title="">Jdlingyu's Ad-wars</a> |
| Advertising / Tracking | ⚪ | 🟩 🟩 🟩 |
| ⚪ | 🟩 | <a href="https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt" title="">AdGuard DNS Filter</a> |
| Tracking | ⚪ | 🔥 🟦 🟩 |
| ⚪ | 🔥 | <a href="https://hostfiles.frogeye.fr/multiparty-trackers-hosts.txt" title="">Geoffrey Frogeye's Multi-Party Trackers</a> |
| ⚪ | 🔥 | <a href="https://raw.githubusercontent.com/Kees1958/W3C_annual_most_used_survey_blocklist/master/EU_US_MV3_most_common_ad%2Btracking_networks.txt" title="">Kees1958's Top Ads & Trackers</a> |
| ⚪ | 🟩 | <a href="https://v.firebog.net/hosts/Prigent-Ads.txt" title="">Fabrice Prigent's Ads</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.2o7Net/hosts" title="">Fademind's 2o7 Network Trackers</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt" title="">Crazy Max's Microsoft Telemetry</a> |
| ⚪ | 🟩 | <a href="https://hostfiles.frogeye.fr/firstparty-trackers-hosts.txt" title="">Geoffrey Frogeye's First-Party Trackers</a> |
| ⚪ | 🟦 | <a href="https://www.github.developerdan.com/hosts/lists/ads-and-tracking-extended.txt" title="">Lightswitch05's Ads & Tracking</a> |
| ⚪ | 🟦 | <a href="https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/android-tracking.txt" title="">Perflyst's Android Trackers</a> |
| ⚪ | 🟦 | <a href="https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/SmartTV.txt" title="">Perflyst's SmartTV Domains</a> |
| ⚪ | 🟦 | <a href="https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/AmazonFireTV.txt" title="">Perflyst's Amazon FireTV Domains</a> |
| ⚪ | 🟦 | <a href="https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt" title="">Quidsup NoTrack Tracker Blocklist</a> |
| Malicious | ⚪ | 🟦 🟩 🟩 |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Alternate%20versions%20Anti-Malware%20List/AntiMalwareHosts.txt" title="">DandelionSprout's Anti-Malware Filter</a> |
| ⚪ | 🟩 | <a href="https://osint.digitalside.it/Threat-Intel/lists/latestdomains.txt" title="">DigitalSide Threat-Intel</a> |
| ⚪ | 🟩 | <a href="https://v.firebog.net/hosts/Prigent-Crypto.txt" title="">Fabrice Prigent's Cryptojacking</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.Risk/hosts" title="">Fademind's Risky Hosts</a> |
| ⚪ | 🟩 | <a href="https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt" title="">Mandiant APT1 Report</a> |
| ⚪ | 🟩 | <a href="https://phishing.army/download/phishing_army_blocklist_extended.txt" title="">Phishing Army's Extended Blocklist</a> |
| ⚪ | 🟩 | <a href="https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt" title="">Quidsup NoTrack Malware Blocklist</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/malware" title="">RPiList Malware</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/Phishing-Angriffe" title="">RPiList Phishing</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/Spam404/lists/master/main-blacklist.txt" title="">Spam404</a> |
| ⚪ | 🟩 | <a href="https://raw.githubusercontent.com/AssoEchap/stalkerware-indicators/master/generated/hosts" title="">AssoEchap's Stalkerware Indicators</a> |
| ⚪ | 🟩 | <a href="https://urlhaus.abuse.ch/downloads/hostfile/" title="">URLhaus Malware URL blocklist</a> |
| ⚪ | 🟦 | <a href="https://malware-filter.gitlab.io/malware-filter/phishing-filter-hosts.txt" title="">Curbengh's Phishing filter</a> |
| ⚪ | 🟦 | <a href="https://v.firebog.net/hosts/Prigent-Malware.txt" title="">Fabrice Prigent's Malware</a> |
| AIO EXCLUSIVE | ⚪ | 🔥 🟦 🟩 |
| ⚪ | 🟩 | <a href="https://gitlab.com/ZeroDot1/CoinBlockerLists/-/raw/master/hosts" title="">ZeroDot1 CoinBlockerLists</a> |
| ⚪ | 🟦 | <a href="https://raw.githubusercontent.com/chadmayfield/my-pihole-blocklists/master/lists/pi_blocklist_porn_top1m.list" title="">Chad Mayfield (Top 1M)</a> |
| ⚪ | 🔥 | [⚪ ⚪ ⚪](https://github.com/KnightmareVIIVIIXC/AdGuard-Home-Allowlist/blob/main/dns_disallowed_domains.txt) |

---

## 📊 The Tests 📊

***Utilities engineered to identify what is being blocked by<br>the selected list of your choice***

| ⚫ |🟢┃🟠┃🔵| ⚫ |
|:---:|:---:|:---:|
|🟡| <a href="https://d3ward.github.io/toolz/" title="">d3ward's Toolz</a> |🟤|
|🟤| <a href="https://adblock-tester.com/" title="">AdBlock Tester</a> |🟣|
|🟣| <a href="https://canyoublockit.com/" title="">Can You Block It</a> |🟡|
| ⚫ |🔵┃🟢┃🟠| ⚫ |
</div>
